# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname="qownnotes"
pkgver=19.11.23
pkgrel=0
pkgdesc="Plain-text file markdown note taking with Nextcloud/ownCloud integration"
url="https://www.qownnotes.org/"
arch="all !ppc64le !s390x"
license="GPL-2.0-only"
makedepends="qt5-qtdeclarative-dev qt5-qtsvg-dev qt5-qtxmlpatterns-dev
	qt5-qttools-dev qt5-qtbase-dev qt5-qtwebsockets-dev botan-dev"
subpackages="$pkgname-lang"
source="https://download.tuxfamily.org/qownnotes/src/qownnotes-${pkgver}.tar.xz"

prepare() {
	default_prepare
	echo "#define RELEASE \"Alpine Linux\"" > release.h
}

build() {
	/usr/lib/qt5/bin/qmake \
		QMAKE_CFLAGS_RELEASE="${CFLAGS}" \
		QMAKE_CXXFLAGS_RELEASE="${CXXFLAGS}" \
		QMAKE_LFLAGS_RELEASE="${LDFLAGS}"
	make
}

check() {
	make check
}

package() {
	install -D -m755 QOwnNotes "${pkgdir}/usr/bin/QOwnNotes"
	install -D -m644 PBE.QOwnNotes.desktop "${pkgdir}/usr/share/applications/PBE.QOwnNotes.desktop"
	install -D -m644 "images/icons/128x128/apps/QOwnNotes.png" "${pkgdir}/usr/share/pixmaps/QOwnNotes.png"
	for format in 16x16 24x24 32x32 48x48 64x64 96x96 128x128 256x256 512x512 ; do
		install -D -m644 "images/icons/${format}/apps/QOwnNotes.png" "${pkgdir}/usr/share/icons/hicolor/$format/apps/QOwnNotes.png"
	done
}

lang() {
	install -d "${subpkgdir}/usr/share/QOwnNotes/languages/"
	install -D -m644 ${builddir}/languages/*.qm "${subpkgdir}/usr/share/QOwnNotes/languages/"
}

sha512sums="7b972095e7218ae9ee8a9c006586d67d1f58176ad57881fb892b3698d2292a02e08c11e68e36532300b70ff490be15bfef709c4dcab93a227b3b3118c279854a  qownnotes-19.11.23.tar.xz"
